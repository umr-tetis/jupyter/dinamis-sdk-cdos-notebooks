# Exemples de notebooks

Exemples de notebooks présentés le 13/06/2023 lors de la réunion 
ATTOS/Ingénierie.

## Pré-requis

Les composants suivants sont requis pour jouer avec les notebooks:

- Docker
- Docker-compose

## Build

La première étape est de construire l'image docker qui servira de base pour le notebook.
Lancer la commande suivante depuis un terminal (linux, mac) ou invite de commandes (windows):

```bash
docker-compose build
```

## Démarrer

Une fois que l'image docker est construite, on peut l'utiliser (sans avoir à la reconstruire à chaque fois).

```bash
docker-compose up
```

Puis ouvrir le lien indiqué dans le navigateur (En général ce lien termine par un token `?token=7dc529f5...7984f83a`).


## Eteindre

Pour éteindre le docker-compose (si on souhaite repartir d'une nouvelle sessions la prochaine fois):

```bash
docker-compose down
```

## Contact

Rémi (docker, docker-compose)
Kenji (notebooks MPC)
